<?php get_header(); ?>
	<div class="row">
		<div class="col-sm-8 blog-main">
			<?php get_template('content', get_post_formate() ); ?>
		</div> <!-- end blog main -->

		<?php get_sidebar(); ?>

	</div> <!-- end row -->
<?php get_footer(); ?>