<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Blog Template for Bootstrap</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head();?>
</head>

<body>
	<div class="social-nav">
		<div>
			<a class="" href="#">About</a>
			<a class="" href="#">FAQ</a>
			<a class="" href="#">Search</a>
		</div>
		<div>
			<a class="" href="#">Instagram</a>
			<a class="" href="#">EMAIL</a>
			<a class="" href="#">Etsy</a>
			<a class="" href="#">Facebook</a>
			<a class="" href="#">Pinterest</a>
		</div>
	</div>
	<div class="blog-masthead">
		<div class="container">
			<nav class="blog-nav">
				<a class="blog-nav-item active" href="#">Home</a>
				<a class="blog-nav-item" href="#">Food</a>
				<a class="blog-nav-item" href="#">Crafts</a>
				<a class="blog-nav-item" href="#">Kids</a>
				<a class="blog-nav-item" href="#">Shop</a>
			</nav>
		</div>
	</div>
	
	<div class="container">

		<div class="blog-header">
			<h1 class="blog-title">The Bootstrap Blog</h1>
			<p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
		</div>